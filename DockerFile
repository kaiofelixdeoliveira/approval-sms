FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/artefato.jar
WORKDIR /app
COPY ${JAR_FILE} /app/app.jar
EXPOSE 80
ENTRYPOINT ["java","-jar","app.jar"]