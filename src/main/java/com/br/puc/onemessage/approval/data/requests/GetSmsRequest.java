package com.br.puc.onemessage.approval.data.requests;

public class GetSmsRequest {

	private String id;
	private String phone;
	private String message;
	private boolean isApproval;
	private boolean isSend;
	private String campaign;
	private String dateSend;
	private String dateTrigger;

	public String getDateSend() {
		return dateSend;
	}

	public String getDateTrigger() {
		return dateTrigger;
	}

	public String getId() {
		return id;
	}

	public String getMessage() {
		return message;
	}

	public String getPhone() {
		return phone;
	}

	public boolean isApproval() {
		return isApproval;
	}

	
	public boolean isSend() {
		return isSend;
	}

	

	public void setApproval(boolean isApproval) {
		this.isApproval = isApproval;
	}

	

	public void setDateSend(String dateSend) {
		this.dateSend = dateSend;
	}

	public void setDateTrigger(String dateTrigger) {
		this.dateTrigger = dateTrigger;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setSend(boolean isSend) {
		this.isSend = isSend;
	}

	public String getCampaign() {
		return campaign;
	}

	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}

	@Override
	public String toString() {
		return "GetSmsRequest [id=" + id + ", phone=" + phone + ", message=" + message + ", isApproval=" + isApproval
				+ ", isSend=" + isSend + ", campaign=" + campaign + ", dateSend=" + dateSend + ", dateTrigger="
				+ dateTrigger + "]";
	}

	

	
}
