package com.br.puc.onemessage.approval.data.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.puc.onemessage.approval.data.requests.NewSmsRequest;
import com.br.puc.onemessage.approval.data.requests.UpdateSmsRequest;
import com.br.puc.onemessage.approval.domain.entities.Message;
import com.br.puc.onemessage.approval.domain.usecases.MessageUseCase;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/message")
public class MessageController {

	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

	@Autowired
	private MessageUseCase messageService;

	@GetMapping(path = "/phone/{phone}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Message>> getMessages(@PathVariable String phone) {
		logger.info("get messages");
		List<Message> messages = new ArrayList<>();
		messages = messageService.getMessages(phone);
		if (messages.isEmpty()) {
			return new ResponseEntity<>(messages, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Message>>(messages, HttpStatus.OK);

	}


	@ApiOperation(value = "Retorna a mensagem pelo id")
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Retorna uma mensagem"), //
			@ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"), //
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),//
	})
	@GetMapping(path = "/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Message> getMessage(@PathVariable Integer id) {
		logger.info("get message");

		Message message = messageService.getMessage(id);

		if (message != null) {
			return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Message>(message, HttpStatus.OK);

	}
	

	@ApiOperation(value = "cria uma nova mensagem")
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "não retorna nada"), //
			@ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"), //
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),//
	})

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> saveMessageForApproval(@RequestBody NewSmsRequest message) {
		logger.info("save message");

		try {
			messageService.saveMessageSms(message);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Error {}", e);
			return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
		}

	}

	@PostMapping(path = "/list", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Message> saveMessagesForApproval(@RequestBody List<Message> message) {
		logger.info("save messages");

		try {
			messageService.saveMessages(message);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			logger.error("ERROR:[{}]",e);
			return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
		}

	}


	@ApiOperation(value = "Retorna uma lista de mensagens")
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Retorna a lista de mensagens"), //
			@ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"), //
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),//
	})
	@GetMapping(path = "/list", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Message>> getMessages() {
		logger.info("get messages");

		try {
			List<Message> message = messageService.getMessages();
			
			return new ResponseEntity<>(message, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("ERROR:[{}]",e);
			return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
		}

	}


	@ApiOperation(value = "Faz a atualização da mensagem")
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Retorna uma mensagem"), //
			@ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"), //
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),//
	})
	@PatchMapping(path = "/update", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Message> updateMessage(@RequestBody UpdateSmsRequest message) {
		logger.info("update message [{}]", message);

		try {
			messageService.updateMessage(message);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			logger.error("ERROR:[{}]",e);
			return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
		}

	}

	@ApiOperation(value = "Faz o disparo de uma mensagem")
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "retorna uma mensagem"), //
			@ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"), //
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),//
	})
	@PostMapping(path = "/send", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Message> sendMessage(@RequestBody UpdateSmsRequest message) {
		logger.info("send message [{}]", message);

		try {
			messageService.sendMessage(message);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			logger.error("ERROR:[{}]",e);
			return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
		}

	}
	
	

}
