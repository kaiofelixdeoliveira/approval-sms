package com.br.puc.onemessage.approval.domain.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.br.puc.onemessage.approval.data.requests.NewSmsRequest;
import com.br.puc.onemessage.approval.data.requests.QueueApprovalRequest;
import com.br.puc.onemessage.approval.data.requests.UpdateSmsRequest;

@Entity(name = "message")
public class Message{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String phone;
	private String message;
	private String description;

	private boolean isSend;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String dateSend;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String dateTrigger;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "approval_id", referencedColumnName = "id")
	private Approval approval;
	
	@ManyToOne
	@JoinColumn(name = "campaign_id", referencedColumnName = "id")
	private Campaign campaign;

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}



	public boolean isSend() {
		return isSend;
	}

	public void setSend(boolean isSend) {
		this.isSend = isSend;
	}

	public String getDateSend() {
		return dateSend;
	}

	public void setDateSend(String dateSend) {
		this.dateSend = dateSend;
	}

	public String getDateTrigger() {
		return dateTrigger;
	}

	public void setDateTrigger(String dateTrigger) {
		this.dateTrigger = dateTrigger;
	}

	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	public void fromNewSmsRequest(NewSmsRequest newSmsRequest) {

		this.dateTrigger = newSmsRequest.getDateTrigger();
		this.message = newSmsRequest.getMessage();
		this.phone = newSmsRequest.getPhone();
		this.campaign = newSmsRequest.getCampaign();
		this.description = newSmsRequest.getDescription();
	}

	public void fromUpdateSmsRequest(UpdateSmsRequest updateSmsRequest) {

		this.id = updateSmsRequest.getId();
		this.approval = updateSmsRequest.getApproval();
		this.dateTrigger = updateSmsRequest.getDateTrigger();
		this.message = updateSmsRequest.getMessage();
		this.phone = updateSmsRequest.getPhone();
		this.campaign = updateSmsRequest.getCampaign();
		this.description = updateSmsRequest.getDescription();
		this.isSend = updateSmsRequest.isSend();
	}

	public void fromQueueApprovalRequest(QueueApprovalRequest queueApprovalRequest) {

		this.dateTrigger = queueApprovalRequest.getDateTrigger();
		this.message = queueApprovalRequest.getMessage();
		this.phone = queueApprovalRequest.getPhone();
		this.campaign = queueApprovalRequest.getCampaign();
		this.description = queueApprovalRequest.getDescription();

	}

	

	public Approval getApproval() {
		return approval;
	}

	public void setApproval(Approval approval) {
		this.approval = approval;
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", phone=" + phone + ", message=" + message + ", description=" + description
				+ ", isSend=" + isSend + ", dateSend=" + dateSend + ", dateTrigger=" + dateTrigger + ", approval="
				+ approval + ", campaign=" + campaign + "]";
	}

	

	


	

}
