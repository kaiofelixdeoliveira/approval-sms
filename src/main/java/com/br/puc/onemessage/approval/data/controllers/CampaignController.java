package com.br.puc.onemessage.approval.data.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.puc.onemessage.approval.domain.entities.Campaign;
import com.br.puc.onemessage.approval.domain.usecases.CampaignUseCase;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/campaign")
public class CampaignController {

	private static final Logger logger = LoggerFactory.getLogger(CampaignController.class);

	@Autowired
	private CampaignUseCase campaignService;


	@ApiOperation(value = "Retorna uma lista de campanhas")
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Retorna a lista de campanhas"), //
			@ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"), //
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),//
	})
	@GetMapping( path="/all",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Campaign>> campaignsMessage() {
		logger.info("get all campaigns message");

		try {
			List<Campaign> campaigns=campaignService.getAll();
			return new ResponseEntity<List<Campaign>>(campaigns, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("ERROR:[{}]",e);
			return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
		}

	}

}
