package com.br.puc.onemessage.approval.domain.usecases;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.br.puc.onemessage.approval.core.utils.DateFormatUtil;
import com.br.puc.onemessage.approval.data.repositories.ApprovalRepository;
import com.br.puc.onemessage.approval.data.requests.ApprovalRequest;
import com.br.puc.onemessage.approval.data.requests.DisapprovalRequest;
import com.br.puc.onemessage.approval.domain.entities.Approval;

@Component
public class ApprovalUseCase {

	public ApprovalRepository approvalRepository;

	@Autowired
	public ApprovalUseCase(ApprovalRepository approvalRepository) {

		this.approvalRepository = approvalRepository;
	}
	
	public void approvalMessage(ApprovalRequest approvalRequest) {

		if (approvalRequest != null && approvalRequest.getId() != null) {
			Optional<Approval> approval = approvalRepository.findById(approvalRequest.getId());

			if (approval.isPresent()) {

				approval.get().setDateApproval(DateFormatUtil.dateNow());
				approval.get().setApproval(true);
				approval.get().setApprover(approvalRequest.getApprover());
				approvalRepository.save(approval.get());

			}
		}

	}

	public void disapprovalMessage(DisapprovalRequest message) {

		if (message != null && message.getId() != null) {
			Optional<Approval> getMessage = approvalRepository.findById(message.getId());

			if (getMessage.isPresent()) {
				getMessage.get().setApproval(false);
				approvalRepository.save(getMessage.get());
			}

		}

	}
}
