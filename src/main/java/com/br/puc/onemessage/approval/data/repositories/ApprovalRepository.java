package com.br.puc.onemessage.approval.data.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.br.puc.onemessage.approval.domain.entities.Approval;

@Repository
public interface ApprovalRepository extends CrudRepository<Approval, Integer>{

	public List<Approval> findByIsApproval(Boolean isApproval);
}
