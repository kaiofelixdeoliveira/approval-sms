package com.br.puc.onemessage.approval.data.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.br.puc.onemessage.approval.domain.entities.Campaign;

@Repository
public interface CampaignRepository extends CrudRepository<Campaign, Integer> {

	public Campaign findByName(String name);
	public Campaign findById(String id);

}
