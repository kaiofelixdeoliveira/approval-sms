package com.br.puc.onemessage.approval.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.br.puc.onemessage.approval.domain.entities.Message;


@Repository
public interface MessageRepository extends CrudRepository<Message, Integer>{
	
	public List<Message> findByPhone(String phone);
	
	@Query("select m from message m inner join approval a where a.isApproval = :isApproval and m.isSend=:isSend")
	public List<Message> findByIsApprovalAndIsSend(@Param("isApproval") Boolean isApproval,@Param("isSend")Boolean isSend);

	
    public Message findByIdAndPhone(String id, String phone);
    



}
