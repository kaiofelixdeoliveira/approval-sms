package com.br.puc.onemessage.approval.core.utils;

public class ApprovalException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ApprovalException(String message) {
		super(message);
	}

	public ApprovalException(String message, Throwable cause) {
		super(message, cause);

	}

	// master alterado
}
