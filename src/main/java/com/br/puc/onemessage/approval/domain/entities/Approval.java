package com.br.puc.onemessage.approval.domain.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

import com.br.puc.onemessage.approval.data.requests.ApprovalRequest;

@Entity(name = "approval")
public class Approval{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String approver;
	private boolean isApproval;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String dateApproval;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getApprover() {
		return approver;
	}
	public void setApprover(String approver) {
		this.approver = approver;
	}
	public boolean isApproval() {
		return isApproval;
	}
	public void setApproval(boolean isApproval) {
		this.isApproval = isApproval;
	}
	public String getDateApproval() {
		return dateApproval;
	}
	public void setDateApproval(String dateApproval) {
		this.dateApproval = dateApproval;
	}
	
	public void fromApprovalRequest(ApprovalRequest approvalRequest) {

		this.id = approvalRequest.getId();
		this.isApproval = approvalRequest.isApproval();
		this.approver = approvalRequest.getApprover();
		this.dateApproval = approvalRequest.getDateApproval();

	}
	@Override
	public String toString() {
		return "Approval [id=" + id + ", approver=" + approver + ", isApproval=" + isApproval + ", dateApproval="
				+ dateApproval + "]";
	}

}
