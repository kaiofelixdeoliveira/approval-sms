package com.br.puc.onemessage.approval;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

import com.br.puc.onemessage.approval.domain.usecases.CampaignUseCase;

@EnableJms
@SpringBootApplication
public class ApprovalApplication implements CommandLineRunner{

	public CampaignUseCase campaignService;
	
	@Autowired
	public ApprovalApplication(CampaignUseCase campaignService) {
		this.campaignService=campaignService;
	}
	public static void main(String[] args) {
		SpringApplication.run(ApprovalApplication.class, args);
	}
	

	
	@Override
    public void run(String... args) throws Exception {
       
		/*
		 * List<Campaign> campaignList= new ArrayList<>();
		 * 
		 * Campaign campaign1= new Campaign(); campaign1.setId(1);
		 * campaign1.setName("Campanha Outubro Rosa"); campaign1.setType("SMS");
		 * 
		 * Campaign campaign2= new Campaign(); campaign2.setId(2);
		 * campaign2.setName("Campanha Novembro Azul"); campaign2.setType("SMS");
		 * 
		 * Campaign campaign3= new Campaign();
		 * campaign3.setId(3);campaign3.setName("Campanha de Natal");
		 * campaign3.setType("SMS");
		 * 
		 * Campaign campaign4= new Campaign(); campaign4.setId(4);
		 * campaign4.setName("empty"); campaign4.setType("empty");
		 * 
		 * campaignList.add(campaign1); campaignList.add(campaign2);
		 * campaignList.add(campaign3); campaignList.add(campaign4);
		 * 
		 * campaignService.save(campaignList);
		 */
		         
    }

}
