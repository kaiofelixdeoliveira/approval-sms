package com.br.puc.onemessage.approval.core.configurations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Component;

@Component
public class WebSecurity extends WebSecurityConfigurerAdapter {

	private final Logger log = LoggerFactory.getLogger(WebSecurity.class);

	public WebSecurity() {
		log.info("WebSecurityConfig init");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()//
		.antMatchers("**").permitAll()//
		.anyRequest().permitAll()//
		
		.and()//
		.csrf().disable();//

	}


}