package com.br.puc.onemessage.approval.data.requests;

import com.br.puc.onemessage.approval.domain.entities.Campaign;

public class NewSmsRequest {
	private String phone;
	private String message;
	private String description;
	private Campaign campaign;
	private String dateTrigger;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDateTrigger() {
		return dateTrigger;
	}

	public void setDateTrigger(String dateTrigger) {
		this.dateTrigger = dateTrigger;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	@Override
	public String toString() {
		return "NewSmsRequest [phone=" + phone + ", message=" + message + ", description=" + description + ", campaign="
				+ campaign + ", dateTrigger=" + dateTrigger + "]";
	}
}
