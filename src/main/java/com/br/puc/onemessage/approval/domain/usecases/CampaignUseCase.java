package com.br.puc.onemessage.approval.domain.usecases;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.puc.onemessage.approval.data.repositories.CampaignRepository;
import com.br.puc.onemessage.approval.domain.entities.Campaign;
import com.google.common.collect.Lists;

@Service
public class CampaignUseCase {

	private final Logger LOGGER = LoggerFactory.getLogger(CampaignUseCase.class);

	public CampaignRepository campaignRepository;

	@Autowired
	public CampaignUseCase(CampaignRepository campaignRepository) {

		this.campaignRepository = campaignRepository;
	}

	public void save(List<Campaign> campaign) {
		LOGGER.info("save campaign [{}]", campaign);
		campaignRepository.saveAll(campaign);
	}

	public List<Campaign> getAll() {

		List<Campaign> campaigns = Lists.newArrayList(campaignRepository.findAll());
		return campaigns;

	}
}
