package com.br.puc.onemessage.approval.data.requests;

import org.springframework.format.annotation.DateTimeFormat;

public class DisapprovalRequest {

	private Integer id;
	private String approver;
	private boolean isApproval;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String dateApproval;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getApprover() {
		return approver;
	}

	public void setApprover(String approver) {
		this.approver = approver;
	}

	public boolean isApproval() {
		return isApproval;
	}

	public void setApproval(boolean isApproval) {
		this.isApproval = isApproval;
	}

	public String getDateApproval() {
		return dateApproval;
	}

	public void setDateApproval(String dateApproval) {
		this.dateApproval = dateApproval;
	}

	@Override
	public String toString() {
		return "ApprovalRequest [id=" + id + ", approver=" + approver + ", isApproval=" + isApproval + ", dateApproval="
				+ dateApproval + "]";
	}

}
