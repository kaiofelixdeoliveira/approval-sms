package com.br.puc.onemessage.approval.core.configurations;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Component;

import com.br.puc.onemessage.approval.data.requests.QueueApprovalRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class ConverterMessage implements MessageConverter {
	private static final Logger LOGGER = LoggerFactory.getLogger(ConverterMessage.class);

	ObjectMapper mapper;

	public ConverterMessage() {
		mapper = new ObjectMapper();
	}

	@Override
	public Message toMessage(Object object, Session session) throws JMSException {

		String payload = null;
		try {
			payload = mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			LOGGER.error("error converting form person", e);
		}

		TextMessage message = session.createTextMessage();
		message.setText(payload);

		return message;
	}

	@Override
	public Object fromMessage(Message message) throws JMSException {
		TextMessage textMessage = (TextMessage) message;
		String payload = textMessage.getText();
		QueueApprovalRequest request = null;
		try {
			request = mapper.readValue(payload, QueueApprovalRequest.class);
		} catch (Exception e) {
			LOGGER.error("error converting to person", e);
		}

		return request;
	}

}
