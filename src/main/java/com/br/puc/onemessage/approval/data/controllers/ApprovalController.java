package com.br.puc.onemessage.approval.data.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.puc.onemessage.approval.data.requests.ApprovalRequest;
import com.br.puc.onemessage.approval.data.requests.DisapprovalRequest;
import com.br.puc.onemessage.approval.domain.usecases.ApprovalUseCase;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin("*")
@RequestMapping("/approval")
public class ApprovalController {

	private static final Logger logger = LoggerFactory.getLogger(ApprovalController.class);

	@Autowired
	private ApprovalUseCase approvalUseCase;
	
	@ApiOperation(value = "Faz a aprovação de mensagens")
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Retorna nada"), //
			@ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"), //
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),//
	})
	@PatchMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> approvalMessage(@RequestBody ApprovalRequest message) {
		logger.info("approval messages");

		try {
			approvalUseCase.approvalMessage(message);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			logger.error("ERROR:[{}]",e);
			return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
		}

	}

	@ApiOperation(value = "Faz a desaprovação de mensagens")
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "Retorna nada"), //
			@ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"), //
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),//
	})
	@PatchMapping(path = "/disapproval", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> disapprovalMessage(@RequestBody DisapprovalRequest message) {
		logger.info("disapproval message");

		try {
			approvalUseCase.disapprovalMessage(message);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			logger.error("ERROR:[{}]",e);
			return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
		}

	}

}
