package com.br.puc.onemessage.approval.core.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatUtil {
	
	public static String dateTimeNow() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date now = new Date();
		String strDate = sdf.format(now);
		
		return strDate;
	}
	
	public static String dateNow() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date now = new Date();
		String strDate = sdf.format(now);
		
		return strDate;
	}

}
