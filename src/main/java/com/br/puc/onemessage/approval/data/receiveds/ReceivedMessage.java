package com.br.puc.onemessage.approval.data.receiveds;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.br.puc.onemessage.approval.data.requests.QueueApprovalRequest;
import com.br.puc.onemessage.approval.domain.entities.Message;
import com.br.puc.onemessage.approval.domain.usecases.MessageUseCase;

@Component
public class ReceivedMessage {

	
	private final Logger logger = LoggerFactory.getLogger(ReceivedMessage.class);
	
	private static final String QUEUE_NAME = "approval-queue";

	
	@Autowired
	private MessageUseCase messageService;

	@JmsListener(destination = QUEUE_NAME, containerFactory = "listenerContainerFactory")
	public void receiveMessage(QueueApprovalRequest request) {
		logger.info("Received message: {}", request.toString());
		Message message=new Message();
		message.fromQueueApprovalRequest(request);
		messageService.saveMessage(message);
		
	}

}
