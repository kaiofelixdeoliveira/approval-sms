package com.br.puc.onemessage.approval.domain.usecases;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.br.puc.onemessage.approval.core.utils.ApprovalException;
import com.br.puc.onemessage.approval.core.utils.CampaignEnum;
import com.br.puc.onemessage.approval.core.utils.DateFormatUtil;
import com.br.puc.onemessage.approval.data.repositories.CampaignRepository;
import com.br.puc.onemessage.approval.data.repositories.MessageRepository;
import com.br.puc.onemessage.approval.data.requests.NewSmsRequest;
import com.br.puc.onemessage.approval.data.requests.UpdateSmsRequest;
import com.br.puc.onemessage.approval.domain.entities.Approval;
import com.br.puc.onemessage.approval.domain.entities.Campaign;
import com.br.puc.onemessage.approval.domain.entities.Message;

@Component
public class MessageUseCase {
	private final Logger LOGGER = LoggerFactory.getLogger(MessageUseCase.class);

	public MessageRepository messageRepository;

	public CampaignRepository campaignRepository;


	@Autowired
	public MessageUseCase(MessageRepository messageRepository,
			CampaignRepository campaignRepository) {

		this.messageRepository = messageRepository;
		this.campaignRepository = campaignRepository;
		
	}

	public List<Message> getMessages(String phone) {

		List<Message> messages = messageRepository.findByPhone(phone);

		if (messages == null) {
			return new ArrayList<>();
		}
		LOGGER.info("Using reactive repository");
		return messages;

	}

	public Message getMessage(Integer id) {
		Optional<Message> message = messageRepository.findById(id);
		message.isPresent();

		LOGGER.info("Using reactive repository");
		return message.isPresent() ? message.get() : new Message();

	}

	public void saveMessageSms(NewSmsRequest newSmsRequest) {

		if (newSmsRequest.getCampaign() != null) {
			Optional<Campaign> campaign = campaignRepository.findById(newSmsRequest.getCampaign().getId());
			if (campaign.isPresent()) {
				newSmsRequest.setCampaign(campaign.get());

			}

		}
		Approval approval = new Approval();
		Message message = new Message();

		approval.setApproval(false);
		message.fromNewSmsRequest(newSmsRequest);
		message.setApproval(approval);
		message.setSend(false);

		if (message != null) {
			messageRepository.save(message);
			LOGGER.info("Mensagem salva com sucesso");

		}

	}

	public void saveMessage(Message message) {

		Approval approval = new Approval();
		approval.setApproval(false);

		message.setApproval(approval);
		message.setSend(false);
		if (message != null) {
			messageRepository.save(message);
			LOGGER.info("Mensagem salva com sucesso");

		}

	}

	public void saveMessages(List<Message> messages) {

		if (!messages.isEmpty()) {
			messageRepository.saveAll(messages);
			LOGGER.info("Mensagens salva com sucesso");
		}

	}

	public List<Message> getMessages() {

		Iterable<Message> messagesIterable = messageRepository.findAll();

		List<Message> messages = new ArrayList<>();

		messagesIterable.forEach(messages::add);

		LOGGER.info(messages.toString());

		return messages;

	}

	public void deleteMessage(Integer id) throws Exception {

		if (id != null) {
			messageRepository.deleteById(id);

		} else {
			LOGGER.error("O id não pode ser nulo");
			throw new ApprovalException("Problema ao tentar apagar o registro no banco");
		}

	}

	public void deleteMessage(Message message) throws Exception {

		if (message != null) {
			messageRepository.delete(message);

		} else {
			LOGGER.error("O Objeto message não pode ser nulo");
			throw new ApprovalException("Problema ao tentar apagar o registro no banco");
		}

	}

	public List<Message> findAllApprovalMessages() {

		Boolean approval = true;
		Boolean send = true;

		List<Message> messages = messageRepository.findByIsApprovalAndIsSend(approval, send);
		if (messages != null) {
			return messages;

		}

		return new ArrayList<>();

	}

	public void updateMessage(UpdateSmsRequest updateSmsRequest) {

		if (updateSmsRequest != null && updateSmsRequest.getId() != null) {
			Optional<Message> getMessage = messageRepository.findById(updateSmsRequest.getId());

			if (getMessage.isPresent()) {
				getMessage.get().fromUpdateSmsRequest(updateSmsRequest);
				if ((getMessage.get().getApproval().getDateApproval() == null //
						|| getMessage.get().getApproval().getDateApproval().isEmpty())//
						&& getMessage.get().getApproval().isApproval()) {//

					getMessage.get().getApproval().setDateApproval(DateFormatUtil.dateNow());
				}
				messageRepository.save(getMessage.get());
			}

		}

	}

	public void sendMessage(UpdateSmsRequest updateSmsRequest) {

		if (updateSmsRequest != null && updateSmsRequest.getId() != null) {
			Optional<Message> getMessage = messageRepository.findById(updateSmsRequest.getId());

			if (getMessage.isPresent()) {
				// getMessage.get().fromUpdateSmsRequest(updateSmsRequest);

				Campaign campaign = campaignRepository.findByName(CampaignEnum.empty.name());

				getMessage.get().setCampaign(campaign);
				getMessage.get().setSend(false);
				getMessage.get().getApproval().setApproval(true);
				getMessage.get().setDateTrigger(null);
				getMessage.get().getApproval().setDateApproval(DateFormatUtil.dateNow());

				messageRepository.save(getMessage.get());
			}

		}

	}

}
