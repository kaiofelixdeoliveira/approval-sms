package com.br.puc.onemessage.approval.data.requests;

import com.br.puc.onemessage.approval.domain.entities.Campaign;

public class QueueApprovalRequest {

	private String message;
	private String description;
	private String phone;
	private Campaign campaign;
	private String dateTrigger;

	public QueueApprovalRequest() {
		super();
	}

	public QueueApprovalRequest(String description,String message, String phone, Campaign campaign, String dateTrigger) {
		super();
		this.message = message;
		this.phone = phone;
		this.campaign = campaign;
		this.dateTrigger = dateTrigger;
		this.description=description;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	

	public String getDateTrigger() {
		return dateTrigger;
	}

	public void setDateTrigger(String dateTrigger) {
		this.dateTrigger = dateTrigger;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	@Override
	public String toString() {
		return "QueueApprovalRequest [message=" + message + ", description=" + description + ", phone=" + phone
				+ ", campaign=" + campaign + ", dateTrigger=" + dateTrigger + "]";
	}

	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	

}
