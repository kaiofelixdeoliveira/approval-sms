package com.br.puc.onemessage.approval.data.requests;

import com.br.puc.onemessage.approval.domain.entities.Approval;
import com.br.puc.onemessage.approval.domain.entities.Campaign;

public class UpdateSmsRequest {
	private Integer id;
	private String phone;
	private String message;
	private String description;
	private boolean isSend;
	private Campaign campaign;
	private Approval approval;
	private String dateSend;
	private String dateTrigger;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	public boolean isSend() {
		return isSend;
	}

	public void setSend(boolean isSend) {
		this.isSend = isSend;
	}

	

	public String getDateSend() {
		return dateSend;
	}

	public void setDateSend(String dateSend) {
		this.dateSend = dateSend;
	}

	public String getDateTrigger() {
		return dateTrigger;
	}

	public void setDateTrigger(String dateTrigger) {
		this.dateTrigger = dateTrigger;
	}

	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}



	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	public Approval getApproval() {
		return approval;
	}

	public void setApproval(Approval approval) {
		this.approval = approval;
	}

	@Override
	public String toString() {
		return "UpdateSmsRequest [id=" + id + ", phone=" + phone + ", message=" + message + ", description="
				+ description + ", isSend=" + isSend + ", campaign=" + campaign + ", approval=" + approval
				+ ", dateSend=" + dateSend + ", dateTrigger=" + dateTrigger + "]";
	}

}
